# 💻 Task-Manager Frontend

This project contains a simple frontend for a task-manager for coding-interviews purposes only. The [task-manager-backend](https://gitlab.com/hofaphil/task-manager-backend) repository can be used as a backend.

If your are doing a coding interview, check out the [conding-interview](link-tbd) branch.

## 🛠️ Project setup
The frontend uses the frameworks vue.js and vuetify. To run this project you need to have node.js installed and run the following steps:

```
// install dependencies
npm install

// hot reload
npm run dev

// only for production build
npm run build
```

The url of the used backend needs to be set in the `TaskApi.ts` class.

## 📡 API
If you want to provide your own backend, the following dto and endpoints need to be provided:

### DTO for communication
```javascript
interface TaskDTO {
  id: number,
  completed: boolean,
  title: string
}
```
### Endpoints
The following endpoints need to be provided by a backend implementation:

---
##### Get all tasks
An endpoint which returns a list of all available tasks.
```
GET /tasks --(returns)--> TaskDTO[]
```
---
##### Update task
This endpoint need to insert or update a task. It then returns the TaskDTO representing the current data base state of the inserted/updated task.
```
POST /tasks --(returns)--> TaskDTO
```
---
##### Delete task
This endpoint deletes a task identified by the provided id.
```
DELETE /tasks/{id} --(returns)--> Void
```
