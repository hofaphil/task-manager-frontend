export default interface TaskDTO {
  id: number,
  completed: boolean,
  title: string
}