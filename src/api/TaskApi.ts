import TaskDTO from "@/model/TaskDTO";

export default class TaskApi {

  private static url = "http://localhost:8080/tasks";

  public static getAllTasks(): Promise<TaskDTO[]> {
    return fetch(TaskApi.url, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response: Response) => {
      if (!response.ok)
        throw new Error(response.status.toString());

      return response.json();
    })
  }

  public static updateTask(task: TaskDTO | { id: number | null }): Promise<TaskDTO> {
    return fetch(TaskApi.url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(task),
    }).then((response: Response) => {
      if (!response.ok)
        throw new Error(response.status.toString());

      return response.json();
    })
  }

  public static deleteTask(id: number): Promise<void> {
    return fetch(TaskApi.url + '/' + id, {
      method: 'DELETE',
    }).then((response: Response) => {
      if (!response.ok)
        throw new Error(response.status.toString());
    })
  }
}